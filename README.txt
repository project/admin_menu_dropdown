Admin Menu Hider toggles the visibility of the Admin Menu with the press of a 
key. You can configure the key under "Admin Menu Hider Settings" at 
admin/settings/admin_menu. You should choose a key you don't use often, so you 
don't toggle the menu by accident. On that page, you can also configure if you 
would  like the menu hidden by default. Normally, the Admin Menu is always 
visible. I recommend you also turn on "Keep Menu at top of Page" above the 
Admin Menu  Hider settings, so you won't have to scroll up to see the menu.

Defaults:
  Hide by default: True
  Key: `
